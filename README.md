# SelectScript_demos

A set of different IPython demonstration notebooks:

* [OpenRAVE]()
* [ICRA-15](http://nbviewer.ipython.org/url/gitlab.com/OvGU-ESS/SelectScript_demos/raw/master/ICRA-15/presentation.ipynb)
* [DSLRob-15](http://nbviewer.ipython.org/url/gitlab.com/OvGU-ESS/SelectScript_demos/raw/master/DSLRob-15/presentation.ipynb)
* [String-Demo](http://nbviewer.ipython.org/url/gitlab.com/OvGU-ESS/SelectScript_demos/raw/master/Misc/SelectScript%20for%20Strings.ipynb)